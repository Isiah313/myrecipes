class DropRecipeStylesTable < ActiveRecord::Migration
  def up
  	drop_table :recipe_styles
  end
  
  def down
    create_table :recipe_styles do |t|
    	t.integer :style_id, :recipe_id
    end
  end
end
