Rails App created in The Professional Rails Development Course by Mashrur Hossain.

### Technologies Used ###

* Ruby 2.1.4
* Rails 4.1.8
* Gravatar
* AWS
* Carrierwave
* Devise
* Bootstrap